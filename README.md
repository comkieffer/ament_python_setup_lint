
# package.xml & setup.py linter

Ensureing that the `package.xml` and `setup.py` files of a ROS package are kept in sync is a chore. This tool aims to automate that process by warning when they are out of sync.


