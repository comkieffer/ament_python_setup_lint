# Copyright 2021 Thibaud Chupin
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


from io import StringIO

from ament_python_setup_lint import parse_setup_file, Person
from .setup_files import setup_files


def test_setup_file_model_ament_lint():
    setup_file = StringIO(setup_files["ament_lint"])

    model = parse_setup_file(setup_file)
    assert model.name == "ament_lint"
    assert model.packages == ["ament_lint"]
    assert model.install_requires == ["setuptools"]
    assert model.zip_safe
    assert model.author == Person("Dirk Thomas", "dthomas@osrfoundation.org")
    assert model.maintainer == [Person("Dirk Thomas", "dthomas@osrfoundation.org")]
    assert model.description == "Providing common API for ament linter packages."
    assert model.license == "Apache License, Version 2.0"
    assert model.tests_require == []
