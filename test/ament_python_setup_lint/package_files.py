# Copyright 2021 Thibaud Chupin
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


# flake8: noqa

package_files = {
    "ament_cmake_core": """
        <?xml version="1.0"?>
        <?xml-model href="http://download.ros.org/schema/package_format2.xsd" schematypens="http://www.w3.org/2001/XMLSchema"?>
        <package format="2">
        <name>ament_cmake_core</name>
        <version>0.9.8</version>
        <description>
            The core of the ament buildsystem in CMake.

            Several subcomponents provide specific funtionalities:
            * environment: provide prefix-level setup files
            * environment_hooks: provide package-level setup files and environment hooks
            * index: store information in an index and retrieve them without crawling
            * package_templates: templates from the ament_package Python package
            * symlink_install: use symlinks for CMake install commands
        </description>
        <maintainer email="dthomas@osrfoundation.org">Dirk Thomas</maintainer>
        <license>Apache License 2.0</license>

        <buildtool_depend>cmake</buildtool_depend>
        <buildtool_depend>ament_package</buildtool_depend>
        <buildtool_depend>python3-catkin-pkg-modules</buildtool_depend>

        <buildtool_export_depend>cmake</buildtool_export_depend>
        <buildtool_export_depend>ament_package</buildtool_export_depend>
        <buildtool_export_depend>python3-catkin-pkg-modules</buildtool_export_depend>

        <export>
            <build_type>ament_cmake</build_type>
        </export>
        </package>
    """
}
