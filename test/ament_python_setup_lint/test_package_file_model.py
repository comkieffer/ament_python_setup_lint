# Copyright 2021 Thibaud Chupin
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


from io import StringIO

from ament_python_setup_lint import parse_package_file, Person

from .package_files import package_files


def test_package_file_model_ament_cmake_core():
    package_file = StringIO(package_files["ament_cmake_core"].strip())  # noqa: E501

    model = parse_package_file(package_file)
    assert model.name == "ament_cmake_core"
    assert model.version == "0.9.8"
    assert model.author is None
    assert model.description.startswith("The core of the ament buildsystem")
    assert model.description.endswith("use symlinks for CMake install commands")
    assert len(model.maintainer) == 1
    assert model.maintainer[0] == Person("Dirk Thomas", "dthomas@osrfoundation.org")
    assert model.license == "Apache License 2.0"
    assert model.buildtool_depend == [
        "cmake",
        "ament_package",
        "python3-catkin-pkg-modules",
    ]
    assert model.buildtool_export_depend == [
        "cmake",
        "ament_package",
        "python3-catkin-pkg-modules",
    ]
