from setuptools import setup

package_name = "ament_python_setup_lint"

setup(
    name=package_name,
    version="0.0.0",
    packages=[package_name],
    data_files=[
        ("share/ament_index/resource_index/packages", ["resource/" + package_name]),
        ("share/" + package_name, ["package.xml"]),
    ],
    install_requires=["setuptools"],
    zip_safe=True,
    maintainer="Thibaud Chupin",
    maintainer_email="thibaud.chupin@gmail.com",
    description="TODO: Package description",
    license="MIT",
    tests_require=["pytest"],
    entry_points={
        "console_scripts": [
            "ament_python_setup_lint = ament_python_setup_lint.__main__.py:main"
        ],
    },
)
