# Copyright 2021 Thibaud Chupin
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


from xml.etree.ElementTree import parse
from dataclasses import dataclass, field
from typing import List, Optional, TextIO

from .models import Person


@dataclass
class PackageFileModel:
    name: Optional[str] = None
    version: Optional[str] = None
    description: Optional[str] = None
    maintainer: List[Person] = field(default_factory=list)
    author: Optional[Person] = None
    license: Optional[str] = None
    buildtool_depend: List[str] = field(default_factory=list)
    buildtool_export_depend: List[str] = field(default_factory=list)
    test_depend: List[str] = field(default_factory=list)
    exec_depend: List[str] = field(default_factory=list)
    export: Optional[str] = None


def parse_package_file(package_file: TextIO) -> PackageFileModel:
    package_xml = parse(package_file)

    model = PackageFileModel()
    root = package_xml.getroot()
    assert root.tag == "package"

    for el in root:
        field_name = el.tag
        if field_name == "maintainer":
            if "email" not in el.attrib:
                print(f"Error: missing attribute 'email' for maintainer '{el.text}'")
                continue
            model.maintainer.append(Person(name=el.text, email=el.attrib["email"]))
        elif field_name == "author":
            if "email" not in el.attrib:
                print(f"Error: missing attribute 'email' for author '{el.text}'")
                continue
            model.author = Person(name=el.text, email=el.attrib["email"])
        else:
            if not hasattr(model, field_name):
                print(f"Not a key: {field_name}")
                continue

            model_field = getattr(model, field_name)
            if isinstance(model_field, list):
                model_field.append(el.text)
            else:
                field_value = el.text
                if isinstance(field_value, str):
                    field_value = field_value.strip()

                setattr(model, field_name, field_value)

    return model
