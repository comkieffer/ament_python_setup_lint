# Copyright 2021 Thibaud Chupin
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


from dataclasses import dataclass, field
from typing import List, Optional, TextIO

from .models import Person


@dataclass
class SetupFileModel:
    name: Optional[str] = None
    version: Optional[str] = None
    packages: List[str] = field(default_factory=list)
    data_files: List[str] = field(default_factory=list)
    install_requires: List[str] = field(default_factory=list)
    zip_safe: Optional[bool] = None
    maintainer: List[Person] = field(default_factory=list)
    author: Optional[Person] = None
    description: Optional[str] = None
    license: Optional[str] = None
    tests_require: List[str] = field(default_factory=list)


def parse_setup_file(setup_file: TextIO) -> SetupFileModel:
    """
    Parse the setup file and build a model of its contents.

    Instead of parsing the file manually (e.g. by walking the AST) we opt for  a
    simpler method. We know that the file __should__ call the `setup` function so we
    mokeypatch it and repalce it with our version.

    This function pre-processes the keyword args a little and forwards them to the
    `SetupFileModel`.

    Args:
        setup_file (StringIO): File-like object containing the contents of the setup
                               file.

    Returns:
        SetupFileModel: The model of the setup file contents.

    """
    # This assignment exists only to trick mypy.
    # The value is overwritten in the `my_setup_fn` function which is called when we
    # exec the setup file.
    model = SetupFileModel()

    def my_setup_fn(*args, **kwargs):
        # We can only parse kwargs. If we get an arg, we won't know what to map it to
        assert len(args) == 0

        if "maintainer" in kwargs.keys() or "maintainer_email" in kwargs.keys():
            kwargs["maintainer"] = [
                Person(
                    kwargs.get("maintainer", None), kwargs.get("maintainer_email", None)
                )
            ]
            del kwargs["maintainer_email"]

        if "author" in kwargs.keys() or "author_email" in kwargs.keys():
            kwargs["author"] = Person(
                kwargs.get("author", None), kwargs.get("author_email", None)
            )

            del kwargs["author_email"]

        # Don't have anything to do with them yet.
        if "entry_points" in kwargs:
            del kwargs["entry_points"]

        # Clean-up the kwargs to remove entries that we don't know about:
        clean_kwargs = {
            k: v for k, v in kwargs.items() if k in SetupFileModel.__dataclass_fields__
        }  # type: ignore

        nonlocal model
        model = SetupFileModel(**clean_kwargs)

    import setuptools

    old_setup_fn = setuptools.setup
    setuptools.setup = my_setup_fn

    exec(setup_file.read())

    setuptools.setup = old_setup_fn

    return model
