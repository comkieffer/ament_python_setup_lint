# Copyright 2021 Thibaud Chupin
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import argparse
import sys

from pathlib import Path

from typing import Optional, List, Tuple


from . import parse_package_file, parse_setup_file, SetupFileModel, PackageFileModel


def compare_models(
    setup_model: SetupFileModel, package_model: PackageFileModel
) -> Tuple[bool, List[str]]:
    report: List[str] = []

    for model_field in setup_model.__dataclass_fields__:  # type: ignore
        if model_field in package_model.__dataclass_fields__:  # type: ignore
            setup_model_value = getattr(setup_model, model_field)
            package_model_value = getattr(package_model, model_field)
            if setup_model_value != package_model_value:
                report.append(
                    (
                        f"Mismatch for key '{model_field}'. Field is "
                        f"'{setup_model_value}' in setup.py and {package_model_value}' "
                        "in package.xml"
                    )
                )

    return not len(report) > 0, report


def main(argv: Optional[List[str]] = None) -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "package_root",
        help="Path to the package to check",
    )

    args = parser.parse_args(argv)

    package_root = Path(args.package_root)
    if not package_root.is_dir():
        print(f"Error: '{package_root}' does not exist or is not a directory.")

    setup_file = package_root / "setup.py"
    if not setup_file.is_file():
        print(f"Error: '{setup_file}' does not exist or is not a file.")
    with setup_file.open() as f:
        setup_model = parse_setup_file(f)

    package_file = package_root / "package.xml"
    if not package_file.is_file():
        print(f"Error: '{package_file}' does not exist or is not a file.")
    with package_file.open() as f:
        package_model = parse_package_file(f)

    success, errors = compare_models(setup_model, package_model)
    if success:
        print('No problems found, "setup.py" matches "package.xml"')
        sys.exit(0)
    else:
        print(f"{len(errors)} errors. 'setup.py' and 'package.xml' differ.")
        for error in errors:
            print(f" - {error}")
        sys.exit(1)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass

    sys.exit(0)
